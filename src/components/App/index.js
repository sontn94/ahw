import React, { useState, useEffect } from 'react'
import { renderRoutes } from "react-router-config";
import './styles.css'
import HomePage from '../HomePage'
import Login from '../Login'
import Register from '../Register'
import Dashboard from '../Dashboard'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { CssBaseline, CircularProgress } from '@material-ui/core'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import firebase from '../firebase'
import LandingPage from '../LandingPage'
import Survey from '../Survey'
import PageNotFount from './404';
import ScrollToTopWhenPathChange from '../Common/scrollTop';
import { createBrowserHistory } from "history";
import Result from '../Result';

const theme = createMuiTheme()
const history = createBrowserHistory();
// export const history = createHistory()

history.listen((location, action) => {
	window.scrollTo(0, 0)
})

export default function App() {
	const [firebaseInitialized, setFirebaseInitialized] = useState(false)

	useEffect(() => {
		firebase.isInitialized().then(val => {
			setFirebaseInitialized(val)
		})
	})

	function handleUpdate() {
		console.log('aaaa')
		let {
			action
		} = this.state.location;

		if (action === 'PUSH') {
			debugger
			window.scrollTo(0, 0);
		}
	}

	return firebaseInitialized !== false ? (
		<MuiThemeProvider theme={theme}>
			<CssBaseline />
			<Router history={history}>
				<ScrollToTopWhenPathChange>
				<Switch>
					<Route exact path="/" component={HomePage} />
					<Route exact path="/login" component={Login} />
					<Route exact path="/register" component={Register} />
					<Route exact path="/landing" component={LandingPage} />
					<Route path={`/survey/:assessmentId`} component={Survey} />
					<Route path={`/result/:assessmentId`} component={Result} />
					<Route path='*' exact={true} component={PageNotFount} />
				</Switch>
				</ScrollToTopWhenPathChange>
			</Router>
		</MuiThemeProvider>
	) : <div id="loader"><CircularProgress /></div>
}

