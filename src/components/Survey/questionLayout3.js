import React, { useEffect, useState } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import { withRouter } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Checkbox from '@material-ui/core/Checkbox';
import _ from 'lodash'

const styles = theme => ({
  paper: {
    marginTop: 57,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'flex-end',
    justifyContent: 'space-between',
    height: 263,
    width: 263,
    textAlign: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  status: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end',
  },
  title: {
    color: '#666666',
    fontSize: 16,
    fontWeight: 'bold'
  },
  answer: {
    backgroundColor: '#F2F2F2',
    padding: 22,
    cursor: 'pointer',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    minHeight: 50,
    fontSize: 16
  },
  question: {
    color: '#333333',
    fontWeight: 'bold',
    fontSize: 20
  },
  active: {
    backgroundColor: '#F2F2F2',
  }
})

function QuestionLayout1(props) {


  const { item, classes, index } = props;
  const question = 'Vivamus laoreet neque nisl, posuere lobortis velit cursus in. Aliquam egestas diam enim?'

  const [listOption, setListOption] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const questionOption = [{
        name: '1',
        content: 'Nam nibh sem, dictum sit amet ex at, pulvinar tempus odio',
        // isCheck: true
      },
      {
        name: '2',
        content: 'Nam nibh sem, dictum sit amet ex at, pulvinar tempus odio',
        // isCheck: true
      },
      {
        name: '3',
        content: 'Nam nibh sem, dictum sit amet ex at, pulvinar tempus odio'
      },
      {
        name: '4',
        content: 'Nam nibh sem, dictum sit amet ex at, pulvinar tempus odio'
      }]
      setListOption(questionOption)
    }
    fetchData();
  }, [])

  useEffect(() => {
    // console.log(listOption)
  })

  // const [answerOfUser, setAnswer] = useState('');

  // const [checked, setChecked] = React.useState(true);

  function setAnswer(answer) {
    const array = _.cloneDeep(listOption);
    const index = array.findIndex(x => x.name === answer.name);
    answer.isCheck = !answer.isCheck;
    array[index] = answer;

    setListOption(array);
  };

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Typography className={classes.question} variant="subtitle1">{`${index + 1}. ${question}`}</Typography>
      </Grid>
      {listOption.map((answer, index) => {
        return <Grid onClick={() => setAnswer(answer)} key={answer.name} item xs={12} sm={12} md={6} lg={6}>
          <div className={[classes.answer]} style={{ backgroundColor: answer.isCheck ? '#D3D3D3' : '#F2F2F2' }}>
            <Checkbox
              color="default"
              checked={answer.isCheck ? true : false}
            // inputProps={{ 'aria-label': 'checkbox with default color' }}
            />
            {answer.content}</div>
        </Grid>
      })}

    </Grid>
  )

}

export default withRouter(withStyles(styles)(QuestionLayout1))