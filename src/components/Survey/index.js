import React, { useEffect, useState } from 'react'
import { Typography, CssBaseline, Button, Box } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import withStyles from '@material-ui/core/styles/withStyles';
import { lighten, makeStyles } from '@material-ui/core/styles';
import firebase from '../firebase'
import { withRouter } from 'react-router-dom';
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import _ from 'lodash';
import { Link } from 'react-router-dom'
import Footer from '../Common/footer';
import Header from '../Common/header';
import QuestionLayout1 from './questionLayout1';
import QuestionLayout2 from './questionLayout2';
import QuestionLayout3 from './questionLayout3';
import LoadingQuestions from '../Common/loadingQuestions';



const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	grid: {
		flexGrow: 1,
		marginBottom: 83
	},

	banner: {
		height: 120,
		backgroundColor: '#C4C4C4',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		position: 'relative'
	},

	paper: {
		marginTop: 57,
		display: 'flex',
		flexDirection: 'column',
		height: 263,
		width: 263,
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
	},
	sumarry: { display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' },
	numberSummary: {
		fontSize: 140,
		color: '#BBBBBB',
		lineHeight: 1,
		textAlign: 'right',
	},
	assessSummary: {
		fontSize: 24,
		color: '#666666',
		textAlign: 'right',
		fontWeight: 500
	},
	submit: {
		width: 253,
		backgroundColor: '#8A8A8A',
		borderRadius: 39,
		marginTop: 20
	},
	bannerTitle: {
		position: 'absolute',
		fontSize: 32,
		bottom: 0,
		color: 'white',
		fontWeight: 'bold'
	},
	sectionTitle: {
		color: '#666666',
		fontSize: 32,
		fontWeight: 'bold',
		textAlign: 'center'
	}
})
const BorderLinearProgress = withStyles({
	root: {
		height: 10,
		backgroundColor: lighten('#ff6c5c', 0.5),
	},
	bar: {
		borderRadius: 20,
		backgroundColor: '#ff6c5c',
	},
})(LinearProgress);

function Survey(props) {
	const id = _.get(props, 'match.params.assessmentId', 0)
	const { classes } = props;

	const [currentSection, setCurrentSection] = useState(0)
	const [listSections, setListSection] = useState([])
	const [listQuestion, setListQuestion] = useState([])
	const [loading, setLoading] = useState(true);

	const [state, setState] = useState({})

	if (!firebase.getCurrentUsername()) {
		// not logged in
		alert('Please login first')
		props.history.replace('/login')
		return null
	}

	useEffect(() => {
		async function data() {
			setLoading(true);
			const listSections = await firebase.getListSectionByAssesstment(id);
			if(listSections) {
				const listQuestion = await firebase.getSectionByID(listSections[0]);
				setListSection(listSections);
				setCurrentSection(0)
				setListQuestion(listQuestion);
				console.log(listQuestion, 'listQuestion')
				
			}
			setLoading(false);
			// console.log(currentSection, 'currentSection')
			// setAssessmentList(assessment)

		}
		data();
	}, [])

	useEffect(() => {
		window.scrollTo(
			{
				top: 0,
				left: 0,
				behavior: 'smooth',
			}
		);
	}, [loading])

	async function nextSection() {
		setLoading(true)
		if (listSections.length - 1 === currentSection) {
			props.history.push(`/result/${id}`)
		} else {
			const listQuestion = await firebase.getSectionByID(listSections[currentSection + 1]);
			console.log(listQuestion, 'listQuestion')
			setCurrentSection(currentSection + 1);
			setListQuestion(listQuestion);
			setLoading(false);
		}
	}

	return (
		<React.Fragment>
			<CssBaseline />
			<Header {...props} title="Animal Health" />
			<div className={classes.banner}>
				<Container fixed maxWidth="lg">
					<Typography className={classes.bannerTitle} variant="h1">
						BREEDING MANAGEMENT
			</Typography>
					<Typography style={{ textAlign: 'center' }} variant="h1">
						Banner
			</Typography>
				</Container>
			</div>
			<Container fixed maxWidth="lg">
				<div className={classes.grid}>
					<Grid style={{ marginTop: 73 }} container spacing={3}>
						<Grid style={{ alignItems: 'center', justifyContent: 'center' }} item xs={12} sm={12} md={12} lg={12}>
							<Container >
								<Typography className={classes.sectionTitle} variant="subtitle1">{listSections[currentSection]}</Typography>
								{/* <Button */}
							</Container>
						</Grid>
					</Grid>
					{loading === true ?
						<Grid container spacing={3}>
							<LoadingQuestions />
						</Grid>
						:
						<Grid container spacing={3}>
							{listQuestion && listQuestion.map((ele, number) => {
								let layout = ''
								if (ele.type === 'muti') {
									layout = <Grid key={ele.id} item xs={12} sm={12} md={12} lg={12}>
										<QuestionLayout3 index={number} />
									</Grid>
								} else if (ele.type === 'single') {
									layout = <Grid key={ele.id} item xs={12} sm={12} md={12} lg={12}>
										<QuestionLayout2 index={number} />
									</Grid>
								} else {
									layout = <Grid key={ele.id} item xs={12} sm={12} md={12} lg={12}>
									<QuestionLayout1 index={number} />
								</Grid>
								}
								return layout
							})}
							{
								!listQuestion &&
								<Grid style={{ alignItems: 'center', justifyContent: 'center' }} item xs={12} sm={12} md={12} lg={12}>
									<Container >
										<Typography className={classes.sectionTitle} variant="subtitle1">There is no question</Typography>
										{/* <Button */}
									</Container>
								</Grid>
							}
							{/* <Grid item xs={12} sm={12} md={12} lg={12}>
						<QuestionLayout2 />

					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12}>
						<QuestionLayout3 />
					</Grid> */}
						</Grid>
					}

				</div>
			</Container>
			{loading === false && listQuestion &&
				<Box style={{ marginBottom: 30, paddingBottom: 20, paddingTop: 20 }} display="flex" justifyContent="center" borderTop={1} borderBottom={1}>
					<Container fixed maxWidth="lg">
						<Grid container spacing={3}>
							<Grid item xs={10} sm={10} md={10} lg={10}>
								<div style={{ backgroundColor: '#E3E3E3', paddingTop: 11, paddingBottom: 11 }}>
									<Container style={{ display: 'flex', justifyContent: 'space-between', }}>
										<Typography style={{ textAlign: 'center' }}>
											Your Progress</Typography>
										<Typography style={{ textAlign: 'center' }}>
											{currentSection / listSections.length * 100}%</Typography>
									</Container>
									<Container>
										<BorderLinearProgress
											className={classes.margin}
											variant="determinate"
											color="secondary"
											value={currentSection / listSections.length * 100}
										/>
									</Container>
								</div>

							</Grid>
							<Grid item xs={2} sm={2} md={2} lg={2}>
								<Button
									// disabled={listSections.length - 1 === currentSection}
									style={{ height: '100%' }}
									type="submit"
									fullWidth
									variant="contained"
									color="default"
									onClick={nextSection}>
									{listSections.length - 1 === currentSection ? 'Finish' : 'Next Section'}
								</Button>
							</Grid>
						</Grid>
					</Container>
				</Box>
			}

			<Footer title="Footer" {...props} />
		</React.Fragment>
	)
}

export default withRouter(withStyles(styles)(Survey))