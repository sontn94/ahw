import React, { useEffect, useState } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import { withRouter } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

const styles = theme => ({
    paper: {
        marginTop: 57,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'flex-end',
        justifyContent: 'space-between',
        height: 263,
        width: 263,
        textAlign: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    status: {
        display: 'flex',
        width: '100%',
        justifyContent: 'flex-end',
    },
    title: {
        color: '#666666',
        fontSize: 16,
        fontWeight: 'bold'
    },
    answer: {
        backgroundColor: '#F2F2F2',
        padding: 22,
        cursor: 'pointer',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        minHeight: 104,
        fontSize: 16
    },
    question: {
        color: '#333333',
        fontWeight: 'bold',
        fontSize: 20
    },
    active: {
        backgroundColor: '#F2F2F2',
    }
})

function QuestionLayout1(props) {


    const { item, classes, index } = props;
    const question = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere, nisi sodales tempor fringilla?'
    const questionOption = [{
        name: 'A',
        content: 'A. Aliquam elementum elit eu lacus malesuada efficitur. Nam nibh sem, dictum sit amet ex at, pulvinar tempus odio. Morbi vestibulum in tortor ac molestie'
    },
    {
        name: 'B',
        content: 'B. Pulvinar tempus odio. Morbi vestibulum in tortor ac molestie'
    },
    {
        name: 'C',
        content: 'C. Nam nibh sem, dictum sit amet ex at, pulvinar tempus odio. Morbi vestibulum in tortor ac molestie'
    },
    {
        name: 'D',
        content: 'D. Nulla facilisi. Aliquam molestie hendrerit felis, quis pellentesque nisl volutpat sit amet. Aenean varius nunc ac enim pharetra maximus. Aenean rhoncus venenatis dui, cursus pellentesque massa iaculis in.'
    }]

    const [answerOfUser, setAnswer] = useState('')
    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
                <Typography className={classes.question} variant="subtitle1">{`${index + 1}. ${question}`}</Typography>
            </Grid>
            {questionOption.map(answer => {
                const color = answerOfUser === answer.name ? 'F2F2F2' : '333333';
                return <Grid onClick={() => setAnswer(answer.name)} key={answer.name} item xs={12} sm={12} md={6} lg={6}>
                    <div className={[classes.answer]} style={{ backgroundColor: answerOfUser === answer.name ? '#D3D3D3' : '#F2F2F2' }}>
                        {answer.content}</div>
                </Grid>
            })}

        </Grid>
    )

}

export default withRouter(withStyles(styles)(QuestionLayout1))