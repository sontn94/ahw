import React, { useEffect, useState } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import { withRouter } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

const styles = theme => ({
    paper: {
        marginTop: 57,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'flex-end',
        justifyContent: 'space-between',
        height: 263,
        width: 263,
        textAlign: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    status: {
        display: 'flex',
        width: '100%',
        justifyContent: 'flex-end',
    },
    title: {
        color: '#666666',
        fontSize: 16,
        fontWeight: 'bold'
    },
    answer: {
        backgroundColor: '#F2F2F2',
        padding: 22,
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        height: 66,
        width: 66,
        fontSize: 16,
        textAlign: 'center',
        justifyContent: 'center'
    },
    question: {
        color: '#333333',
        fontWeight: 'bold',
        fontSize: 20
    },
    active: {
        backgroundColor: '#F2F2F2',
    }
})

function QuestionLayout2(props) {


    const { item, classes, index } = props;
    const question = `Fusce ut pretium lacus. Etiam non blandit nulla. Pellentesque ut nunc erat. Nam malesuada diam ac ante accumsan,
    efficitur consectetur turpis convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit?`
    const questionOption = [{
        name: '1',
        content: '1'
    },
    {
        name: '2',
        content: '2'
    },
    {
        name: '3',
        content: '3'
    },
    {
        name: '4',
        content: '4'
    },
    {
        name: '5',
        content: '5'
    },
    {
        name: '6',
        content: '6'
    },
    {
        name: '7',
        content: '7'
    },
    {
        name: '8',
        content: '8'
    },
    {
        name: '9',
        content: '9'
    },
    {
        name: '10',
        content: '10'
    },]

    const [answerOfUser, setAnswer] = useState('')
    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
                <Typography className={classes.question} variant="subtitle1">{`${index + 1}. ${question}`}</Typography>
            </Grid>
            {questionOption.map(answer => {
                const color = answerOfUser === answer.name ? 'F2F2F2' : '333333';
                return <Grid onClick={() => setAnswer(answer.name)} key={answer.name} item xs={4} sm={4} md={1} lg={1}>
                    <div className={[classes.answer]} style={{ backgroundColor: answerOfUser === answer.name ? '#D3D3D3' : '#F2F2F2' }}>
                        {answer.name}</div>
                </Grid>
            })}

        </Grid>
    )

}

export default withRouter(withStyles(styles)(QuestionLayout2))