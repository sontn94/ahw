import React, { useEffect, useState } from 'react'
import { Typography, CssBaseline, Button } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import withStyles from '@material-ui/core/styles/withStyles';
import LinearProgress from '@material-ui/core/LinearProgress';
import firebase from '../firebase'
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Header from '../Common/header';
import Footer from '../Common/footer';
import AssessMentItem from './assessmentItem';


const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	grid: {
		flexGrow: 1,
		marginBottom: 83
	},

	banner: {
		height: 401,
		backgroundColor: '#E0E0E0',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},

	paper: {
		marginTop: 57,
		display: 'flex',
		flexDirection: 'column',
		height: 263,
		width: 263,
		backgroundColor: '#E8E8E8',
		borderRadius: 0,
		position: 'relative'
	},
	sumarry: { display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' },
	numberSummary: {
		fontSize: 140,
		color: '#BBBBBB',
		lineHeight: 1,
		textAlign: 'right',
	},
	assessSummary: {
		fontSize: 24,
		color: '#666666',
		textAlign: 'right',
		fontWeight: 500
	},
	submit: {
		width: 360,
		height: 60,
		margin: '24px 50px',
		backgroundColor: '#878787',
		borderRadius: 0,
		boxShadow: 'none',
		'&:hover': {
			background: "#C4C4C4",
			boxShadow: 'none',
		 },
	},
	modulesInfo:{
		display:  'flex',
		flexDirection: 'row',
		padding: 0,
		fontFamily: 'roboto',
		paddingRight: '27px',
		justifyContent: 'flex-end'
	},
	completedModulesNum:{
		color: '#49C155',
		fontSize: 120,
		fontWeight: 'bold',
		lineHeight: '141px',
		position: 'relative',
	},
	totalModulesNum: {
		fontSize: 80,
		color: '#BBBBBB',
		fontWeight: 'bold',
		lineHeight: '94px',
		position: 'relative',
		letterSpacing: '-0.085em'
	},
	modulesText:{
		fontSize: 24,
		lineHeight: '28px',
		paddingRight: 27,
		color: '#666666',
		fontWeight: 'bold',
		textAlign: 'end'
	},
	processArea: {
		position: 'absolute',
		bottom: 0,
		padding: 3,
		width: '100%'
	},
	processBar: {
		height: 13,
		width: '100%',
		color: '#939393'
	},
	btnGroup:{
		marginTop: '56px'
	},
	bannerText:{
		color: '#BFBFBF',
		fontSize: '60px',
		textTransform: 'uppercase'
	}
})

const BorderLinearProgress = withStyles({
	root: {
		height: 13,
		backgroundColor: '#FFFFFF',
	},
	bar: {
		borderRadius: 0,
		backgroundColor: '#939393',
	},
})(LinearProgress);

function Dashboard(props) {
	const { classes } = props;
	if (!firebase.getCurrentUsername()) {
		// not logged in
		alert('Please login first')
		props.history.replace('/login')
		return null
	}

	// const [quote, setQuote] = useState('')

	useEffect(() => {
		async function fetchData() {
			const email = firebase.auth.currentUser.email;
			const assessment = await firebase.getAssessments(email);
			console.log(assessment, 'assessment')
			setAssessmentList(assessment)
		}
		fetchData();
	}, [])

	const [listAssessment, setAssessmentList] = useState('')
	return (
		<React.Fragment>
			<CssBaseline />
			<Header {...props} title="Animal Health" />
			<div className={classes.banner}>
				<Typography className={classes.bannerText} variant="h1">
					Banner
				</Typography>
			</div>
			<Container fixed maxWidth="lg">

				<div className={classes.grid}>
					<Grid container spacing={3} style={{ marginTop: 73 }}>
						<Grid item xs={6} sm={6} md={4} lg={3}>
							<Paper className={classes.paper} elevation={0}>
								<div className={classes.modulesInfo}>
									<Typography className={classes.completedModulesNum}  variant="subtitle1">5</Typography>
									<Typography className={classes.totalModulesNum} style={{fontStyle: 'italic', marginTop: '34px'}} variant="subtitle1">/</Typography>
									<Typography className={classes.totalModulesNum} style={{marginTop: '54px', marginLeft: '8px'}} variant="subtitle1">11</Typography>
								</div>
								<Typography className={classes.modulesText} variant="subtitle1">COMPLETED MODULES</Typography>
								<div className={classes.processArea}>
								<BorderLinearProgress
									className= {classes.processBar}
									variant="determinate"
									color="secondary"
									value={ 5 / 11 * 100}
								/>
								</div>
							</Paper>

						</Grid>
						{listAssessment && listAssessment.map((item, index) => {
							item.index = index
							return <Grid onClick={() => redirectSurvey(item)} key={index} item xs={6} sm={6} md={4} lg={3}>
								<AssessMentItem item={item} />
							</Grid>
						})}
					</Grid>
					<Grid spacing={3} item xs={12} sm={12} md={12} lg={12}
						container
						direction="row"
						justify="center"
						alignItems="center"
						className={classes.btnGroup}
					>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="secondary"
							onClick={clickButton}
							className={classes.submit}>
							TREATMENT REPORT</Button>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="secondary"
							onClick={clickButton}
							className={classes.submit}>
							ACTION PLAN</Button>
						</Grid>
				</div>
			</Container>
			<Footer title="Footer"{...props} />
		</React.Fragment>
	)

	function redirectSurvey(item) {
		console.log(props, 'props.history')
		props.history.push(`/survey/${item.id}`)
	}

	async function logout() {
		await firebase.logout()
		props.history.push('/')
	}

	async function clickButton() {
		// setAssessmentList([])
	}
}

export default withRouter(withStyles(styles)(Dashboard))