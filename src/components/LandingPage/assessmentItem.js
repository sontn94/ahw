import React, { useEffect, useState } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import { withRouter } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = theme => ({
    paper: {
        cursor: 'pointer',
        marginTop: 57,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'flex-end',
        justifyContent: 'space-between',
        height: 263,
        width: 263,
        textAlign: 'center',
        padding: `0 ${theme.spacing.unit * 1}px 0`,
        borderRadius: 0,
        backgroundColor: '#E8E8E8',
        position: 'relative',
        paddingTop: 0
    },
    status: {
        display: 'flex',
        width: '100%',
        justifyContent: 'flex-end',
        position: 'absolute',
        padding: '12px 17px'
    },
    assessmentTittle: {
        width: '100%',
        overflow: 'hidden',
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        color: '#666666',
        fontSize: 16,
        fontWeight: 'bold',
        padding: '0 16px'
    },
    desc:{
        color: '#666666',
        fontSize: 12,
        padding: 0
    },
    processArea: {
		position: 'absolute',
		bottom: 0,
		padding: 3,
		width: '100%'
	},
	processBar: {
		height: 4,
		width: '100%',
		color: '#939393'
    },
   
})

const BorderLinearProgress = withStyles({
	root: {
		height: 4,
		backgroundColor: '#FFFFFF',
	},
	bar: {
		borderRadius: 0,
		backgroundColor: '#B7B7B7',
	},
})(LinearProgress);

const SuccessfulBorderLinearProgress = withStyles({
	root: {
		height: 4,
		backgroundColor: '#FFFFFF',
	},
	bar: {
		borderRadius: 0,
		backgroundColor: '#49C155',
	},
})(LinearProgress);


function AssessMentItem(props) {
    const { item, classes } = props;
    return (
        <Paper className={classes.paper} elevation={0}>
            <div className={classes.status}>
                {item.status === true ? <img alt="Check" src={require("../../asset/check.svg")} />: item.status === null? <img alt="Check" src={require("../../asset/notyet.svg")} />
                :<img alt="Check" src={require("../../asset/warning.svg")} />}
            </div>
            <div>
                <img alt={item.name} src={require("../../asset/default-thumbnail.png")}></img>
            </div>
            <div className={classes.assessmentTittle}>
            <div className={classes.title}>
                {`${item.index + 1}. ${item.name}`}
            </div>
            <div className={classes.desc}>{item.description}</div>
            </div>
           
            <div className={classes.processArea}>
                {
                    item.status === true? 
                    <SuccessfulBorderLinearProgress
                    className= {classes.processBar}
                    variant="determinate"
                    color="secondary"
                    value={100}
                />:
                    <BorderLinearProgress
                    className= {classes.processBar}
                    variant="determinate"
                    color="secondary"
                    value={item.status === null ? 0 : 50}
                />
                }
                
			</div>
        </Paper>
    )

}

export default withRouter(withStyles(styles)(AssessMentItem))