import React, { useEffect, useState } from 'react'
import { Typography, CssBaseline, Button, Box } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import withStyles from '@material-ui/core/styles/withStyles';
import { lighten, makeStyles } from '@material-ui/core/styles';
import firebase from '../firebase'
import { withRouter } from 'react-router-dom';
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import _ from 'lodash';
import { Link } from 'react-router-dom'
import Footer from '../Common/footer';
import Header from '../Common/header';



const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    grid: {
        flexGrow: 1,
        marginBottom: 83
    },

    banner: {
        height: 120,
        backgroundColor: '#C4C4C4',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
    },

    paper: {
        marginTop: 57,
        display: 'flex',
        flexDirection: 'column',
        height: 263,
        width: 263,
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    sumarry: { display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' },
    numberSummary: {
        fontSize: 140,
        color: '#BBBBBB',
        lineHeight: 1,
        textAlign: 'right',
    },
    assessSummary: {
        fontSize: 24,
        color: '#666666',
        textAlign: 'right',
        fontWeight: 500
    },
    submit: {
        width: 253,
        backgroundColor: '#8A8A8A',
        borderRadius: 39,
        marginTop: 20
    },
    bannerTitle: {
        position: 'absolute',
        fontSize: 32,
        bottom: 0,
        color: 'white',
        fontWeight: 'bold'
    },
    sectionTitle: {
        color: '#666666',
        fontSize: 32,
        fontWeight: 'bold',
        textAlign: 'center'
    }
})
const BorderLinearProgress = withStyles({
    root: {
        height: 10,
        backgroundColor: lighten('#ff6c5c', 0.5),
    },
    bar: {
        borderRadius: 20,
        backgroundColor: '#ff6c5c',
    },
})(LinearProgress);

function Result(props) {
    const id = _.get(props, 'match.params.assessmentId', 0)
    const { classes } = props;

    const [currentSection, setCurrentSection] = useState(0)
    const [listSections, setListSection] = useState([])
    const [listQuestion, setListQuestion] = useState([])
    const [loading, setLoading] = useState(true);

    const [state, setState] = useState({})

    if (!firebase.getCurrentUsername()) {
        // not logged in
        alert('Please login first')
        props.history.replace('/login')
        return null
    }

    useEffect(() => {
        async function data() {
            setLoading(true);
            const listSections = await firebase.getListSectionByAssesstment(id);
            const listQuestion = await firebase.getSectionByID(listSections[0]);
            setListSection(listSections);
            setCurrentSection(0)
            setListQuestion(listQuestion);
            console.log(listQuestion, 'listQuestion')
            setLoading(false);
            // console.log(currentSection, 'currentSection')
            // setAssessmentList(assessment)

        }
        data();
    }, [])

    useEffect(() => {
        window.scrollTo(
            {
                top: 0,
                left: 0,
                behavior: 'smooth',
            }
        );
    }, [loading])

    async function nextSection() {
        setLoading(true)
        if (listSections.length - 1 === currentSection) {
            props.history.push(`/landing`)
        } else {
            const listQuestion = await firebase.getSectionByID(listSections[currentSection + 1]);
            console.log(listQuestion, 'listQuestion')
            setCurrentSection(currentSection + 1);
            setListQuestion(listQuestion);
            setLoading(false);
        }
    }

    return (
        <React.Fragment>
            <CssBaseline />
            <Header {...props} title="Animal Health" />
            <div className={classes.banner}>
                <Container fixed maxWidth="lg">
                    <Typography className={classes.bannerTitle} variant="h1">
                        BREEDING MANAGEMENT
			</Typography>
                    <Typography style={{ textAlign: 'center' }} variant="h1">
                        Banner
			</Typography>
                </Container>
            </div>
            <Container fixed maxWidth="lg">
                <div className={classes.grid}>
                    <Grid style={{ marginTop: 73 }} container spacing={3}>
                        <Grid style={{ alignItems: 'center', justifyContent: 'center' }} item xs={12} sm={12} md={12} lg={12}>
                            <Container >
                                <Typography className={classes.sectionTitle} variant="subtitle1">Result Page</Typography>
                                {/* <Button */}
                            </Container>
                        </Grid>
                    </Grid>
                    <Grid style={{minHeight: 200}} container spacing={3}>
                        {/* <Typography className={classes.sectionTitle} variant="subtitle1">Result Page</Typography> */}
                    </Grid>

                </div>
            </Container>
            }

            <Footer title="Footer" {...props} />
        </React.Fragment>
    )
}

export default withRouter(withStyles(styles)(Result))