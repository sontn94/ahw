import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  lazyLoading: {
      paddingTop: '10px',
      paddingBottom: '10px'
  }
});

export default function LoadingQuestions() {
  const classes = useStyles();

  const renderLazyLoading = () => {
      var element = [];
      for(var i = 0; i< 3; i++){
          element.push(
            <div className = {classes.lazyLoading}>
            <Skeleton />
            <Skeleton animation={false} />
            <Skeleton animation="wave" />
            <Skeleton animation={false} />
            <Skeleton animation="wave" />
        </div>);
      }
      return element;
  }

  return (
    <div className={classes.root}>
        {
            renderLazyLoading()
        }
       

    </div>
  );
}